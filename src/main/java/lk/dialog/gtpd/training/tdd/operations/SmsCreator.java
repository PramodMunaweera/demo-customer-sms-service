package lk.dialog.gtpd.training.tdd.operations;

public class SmsCreator {

    /**
     * Gets the content of the SMS to be sent to the customer.
     *   The content varies by time of day.
     */
    public String getSmsContent() {
        return "Thank you for visiting Dialog this morning";
    }
}
